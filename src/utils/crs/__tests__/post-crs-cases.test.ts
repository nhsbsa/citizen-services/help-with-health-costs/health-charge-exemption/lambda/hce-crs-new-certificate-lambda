import { HttpRequestMethod } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { postCrsCases } from "../post-crs-cases";
import { CrsPostRequest } from "../schemas";
import { infoSpy } from "../../../../test-setup/setup";
import { mockData } from "../../../__mocks__";
import { CrsApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

let apiSpy: jest.SpyInstance;
const mockCrsRequest = {
  caseRefId: "123",
} as CrsPostRequest;

beforeAll(() => {
  apiSpy = jest.spyOn(CrsApi.prototype, "makeRequest").mockResolvedValue({});
});

describe("postCrsCases()", () => {
  test("should call CrsApi with the correct parameters", async () => {
    // given / when
    await postCrsCases(mockCrsRequest, mockData.apiHeaders);

    // then
    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(apiSpy).toHaveBeenCalledWith({
      method: HttpRequestMethod.POST,
      url: `/cases`,
      headers: mockData.apiHeaders,
      data: mockCrsRequest,
    });
  });

  test("should throw an error when CrsApi error occurs", async () => {
    // given
    apiSpy.mockImplementation(() => {
      throw new Error();
    });

    // when
    await expect(() =>
      postCrsCases(mockCrsRequest, mockData.apiHeaders),
    ).rejects.toThrow(new Error());

    // then
    expect(infoSpy).toHaveBeenCalledTimes(1);
    expect(infoSpy).toHaveBeenCalledWith(
      "Posting to the CRS API for certificate",
      mockCrsRequest.caseRefId,
    );
    expect(apiSpy).toHaveBeenCalledTimes(1);
  });
});
