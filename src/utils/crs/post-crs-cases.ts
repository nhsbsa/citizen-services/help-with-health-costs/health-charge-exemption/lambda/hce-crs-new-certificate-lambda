import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  CrsApi,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { HttpRequestMethod } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { CrsPostRequest } from "./schemas";

const logger = loggerWithContext();

export const postCrsCases = async (
  crsRequest: CrsPostRequest,
  headers: MandatoryHeaders,
): Promise<unknown> => {
  logger.info("Posting to the CRS API for certificate", crsRequest.caseRefId);

  return new CrsApi().makeRequest({
    method: HttpRequestMethod.POST,
    url: `/cases`,
    headers,
    data: crsRequest,
  });
};
