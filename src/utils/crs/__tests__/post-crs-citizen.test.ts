import { HttpRequestMethod } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { postCrsCitizen } from "../post-crs-citizen";
import { CrsUpdateRequest } from "../schemas";
import { infoSpy } from "../../../../test-setup/setup";
import { mockData } from "../../../__mocks__";
import { CrsApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

let apiSpy: jest.SpyInstance;
const mockCrsRequest = {
  caseRefId: "123",
} as CrsUpdateRequest;

beforeAll(() => {
  apiSpy = jest.spyOn(CrsApi.prototype, "makeRequest").mockResolvedValue({});
});

describe("postCrsCitizen()", () => {
  test("should call CrsApi with the correct parameters", async () => {
    // given / when
    await postCrsCitizen(mockCrsRequest, mockData.apiHeaders);

    // then
    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(apiSpy).toHaveBeenCalledWith({
      method: HttpRequestMethod.POST,
      url: `/crs-citizen`,
      headers: mockData.apiHeaders,
      data: mockCrsRequest,
    });
  });

  test("should throw an error when CrsApi error occurs", async () => {
    // given
    apiSpy.mockImplementation(() => {
      throw new Error();
    });

    // when
    await expect(() =>
      postCrsCitizen(mockCrsRequest, mockData.apiHeaders),
    ).rejects.toThrow(new Error());

    // then
    expect(infoSpy).toHaveBeenCalledTimes(1);
    expect(infoSpy).toHaveBeenCalledWith(
      "Posting update to the CRS API for certificate",
      mockCrsRequest.caseRefId,
    );
    expect(apiSpy).toHaveBeenCalledTimes(1);
  });
});
