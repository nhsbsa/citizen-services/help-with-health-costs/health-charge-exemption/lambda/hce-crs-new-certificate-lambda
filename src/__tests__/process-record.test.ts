import { processRecord } from "../process-record";
import * as logger from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  handleCertificateUpdate,
  handleCitizenUpdate,
} from "../event-type-handler";
import * as validateAndGetMessageAttribute from "../utils/get-message-attribute";
import { CONSTANTS } from "../utils/constants";
import { mockData } from "../__mocks__";
import { infoSpy, errorSpy } from "../../test-setup/setup";

const correlationId = mockData.correlationId;
const logMessageStarted = "Starting message processing";
const logMessageSuccessful = "Successfully processed the message";

let validateAndGetMessageAttributeSpy: jest.SpyInstance;
let setCorrelationIdSpy: jest.SpyInstance;

jest.mock("../event-type-handler");

const mockHandleCitizenUpdate = handleCitizenUpdate as jest.MockedFunction<
  typeof handleCitizenUpdate
>;

beforeAll(() => {
  setCorrelationIdSpy = jest.spyOn(logger, "setCorrelationId");
  validateAndGetMessageAttributeSpy = jest.spyOn(
    validateAndGetMessageAttribute,
    "validateAndGetMessageAttribute",
  );
});

describe("processRecord()", () => {
  test.each([
    ["certificate update with hc3", mockData.certificateUpdateHc3SqsRecord],
    ["certificate update with hc2", mockData.certificateUpdateHc2SqsRecord],
  ])("should process '%s' record successfully", async (_, sqsRecord) => {
    // given
    validateAndGetMessageAttributeSpy.mockReturnValueOnce(correlationId);

    // when
    await expect(processRecord(sqsRecord)).resolves.not.toThrow();

    // then
    const infoLogAttributes = {
      messageId: sqsRecord.messageId,
    };
    verifyCallToValidateAndGetMessageAttribute(sqsRecord);
    expect(handleCertificateUpdate).toHaveBeenCalledTimes(1);
    expect(handleCertificateUpdate).toHaveBeenCalledWith(
      sqsRecord,
      correlationId,
    );
    expect(infoSpy).toHaveBeenCalledTimes(2);
    expect(infoSpy).toHaveBeenNthCalledWith(
      1,
      logMessageStarted,
      infoLogAttributes,
    );
    expect(infoSpy).toHaveBeenNthCalledWith(
      2,
      logMessageSuccessful,
      infoLogAttributes,
    );
    expect(errorSpy).not.toHaveBeenCalled();
  });

  test("should process 'citizen-update' event type successfully", async () => {
    // given
    const sqsRecord = mockData.citizenUpdateSqsRecord;
    validateAndGetMessageAttributeSpy.mockReturnValueOnce(correlationId);

    // when
    await expect(processRecord(sqsRecord)).resolves.not.toThrow();

    // then
    const infoLogAttributes = {
      messageId: sqsRecord.messageId,
    };
    verifyCallToValidateAndGetMessageAttribute(sqsRecord);
    expect(handleCitizenUpdate).toHaveBeenCalledTimes(1);
    expect(handleCitizenUpdate).toHaveBeenCalledWith(sqsRecord, correlationId);
    expect(infoSpy).toHaveBeenCalledTimes(2);
    expect(infoSpy).toHaveBeenNthCalledWith(
      1,
      logMessageStarted,
      infoLogAttributes,
    );
    expect(infoSpy).toHaveBeenNthCalledWith(
      2,
      logMessageSuccessful,
      infoLogAttributes,
    );
    expect(errorSpy).not.toHaveBeenCalled();
  });

  test("should log when event type is not supported and process the event without call to crs functions", async () => {
    // given
    const eventType = "unsupported-event-type";
    const sqsRecord = { ...mockData.citizenUpdateSqsRecord };
    const body = JSON.parse(sqsRecord.body);
    sqsRecord.body = JSON.stringify({ ...body, eventType });

    validateAndGetMessageAttributeSpy.mockReturnValueOnce(correlationId);

    // when
    await expect(processRecord(sqsRecord)).resolves.not.toThrow();

    // then
    const logAttributes = {
      messageId: sqsRecord.messageId,
    };
    verifyCallToValidateAndGetMessageAttribute(sqsRecord);
    expect(handleCitizenUpdate).not.toHaveBeenCalled();
    expect(handleCertificateUpdate).not.toHaveBeenCalled();
    expect(infoSpy).toHaveBeenCalledTimes(3);
    expect(infoSpy).toHaveBeenNthCalledWith(
      1,
      logMessageStarted,
      logAttributes,
    );
    expect(infoSpy).toHaveBeenNthCalledWith(
      2,
      `Unsupported event type: ${eventType}, skipping call to CRS`,
    );
    expect(infoSpy).toHaveBeenNthCalledWith(
      3,
      "Successfully processed the message",
      logAttributes,
    );
    expect(errorSpy).not.toHaveBeenCalled();
  });

  test.each([
    ["mock update with hc3", mockData.certificateUpdateHc3SqsRecord],
    ["mock update with hc2", mockData.certificateUpdateHc2SqsRecord],
  ])(
    "should throw error when error captured in update function for %s",
    async (_, sqsRecord) => {
      // given
      const errorMessage = "Internal Server Error";
      validateAndGetMessageAttributeSpy.mockReturnValueOnce(correlationId);
      mockHandleCitizenUpdate.mockRejectedValue(new Error(errorMessage));
      const body = JSON.parse(sqsRecord.body);
      sqsRecord.body = JSON.stringify({
        ...body,
        eventType: CONSTANTS.CITIZEN_UPDATE,
      });

      // when
      await expect(() => processRecord(sqsRecord)).rejects.toThrow(
        errorMessage,
      );

      // then
      const logAttributes = {
        messageId: sqsRecord.messageId,
      };
      verifyCallToValidateAndGetMessageAttribute(sqsRecord);
      expect(handleCitizenUpdate).toHaveBeenCalledTimes(1);
      expect(handleCertificateUpdate).not.toHaveBeenCalled();
      expect(infoSpy).toHaveBeenCalledTimes(1);
      expect(infoSpy).toHaveBeenNthCalledWith(
        1,
        logMessageStarted,
        logAttributes,
      );
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toHaveBeenNthCalledWith(1, "Failed to process message", {
        errorMessage: errorMessage,
        ...logAttributes,
      });
    },
  );
});

function verifyCallToValidateAndGetMessageAttribute(sqsRecord) {
  expect(validateAndGetMessageAttributeSpy).toHaveBeenCalledTimes(1);
  expect(validateAndGetMessageAttributeSpy).toHaveBeenCalledWith(
    sqsRecord,
    CONSTANTS.CORRELATION_ID,
  );
  expect(setCorrelationIdSpy).toHaveBeenCalledTimes(1);
  expect(setCorrelationIdSpy).toHaveBeenCalledWith(correlationId);
}
