import { getHeaders, getCitizen, getActiveCertificates } from "../utils";
import { SQSRecord } from "aws-lambda";
import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  LisCertificateTypes,
  CertificateUpdateSqsBody,
  CertificateResponse,
  CitizenGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";
import { postCrsCases } from "../utils/crs/post-crs-cases";
import { transformCertificateToCrsRequest } from "../utils/crs/crs-request-mapping";

const logger = loggerWithContext();

// This function is responsible for handling the certificate update event
export const handleCertificateUpdate = async (
  record: SQSRecord,
  correlationId: string,
) => {
  logger.info(
    "Received and starting to process the certificate update event type",
  );

  const headers = getHeaders(correlationId);
  const body: CertificateUpdateSqsBody = JSON.parse(record.body);

  if (!(body.certificate.type in LisCertificateTypes)) {
    const message = `Received unsupported certificate type: ${body.certificate.type}`;
    throw new Error(message);
  }

  // First citizen api call
  const citizenResponse = await getCitizen(body.citizenId, headers);

  if (!citizenResponse || !citizenResponse.id) {
    const message = `No citizen found for citizenId: ${body.citizenId}, certificateId: ${body.certificate.id}`;
    throw new Error(message);
  }

  const partnerCitizenId = body.lowIncomeScheme?.partnerCitizenId;
  let citizenIds = body.citizenId;
  let partnerResponse: CitizenGetResponse;
  if (partnerCitizenId) {
    // Second citizen api call for the partner information
    partnerResponse = await getCitizen(partnerCitizenId, headers);

    if (!partnerResponse || !partnerResponse.id) {
      const message = `No partner found for partnerCitizenId: ${partnerCitizenId}, certificateId: ${body.certificate.id}`;
      throw new Error(message);
    }
    citizenIds = citizenIds.concat(",", partnerCitizenId);
  }

  // Certificate api call
  const certificates: CertificateResponse[] = await getActiveCertificates(
    citizenIds,
    headers,
  );

  await Promise.all(
    certificates.map(async (certificate) => {
      try {
        const crsRequest = transformCertificateToCrsRequest(
          citizenResponse,
          partnerResponse,
          certificate,
        );
        await postCrsCases(crsRequest, headers);
      } catch (error) {
        logger.error(
          `Failed to POST to the CRS API for certificate id: ${certificate.id}`,
        );
        throw error;
      }
    }),
  );
};
