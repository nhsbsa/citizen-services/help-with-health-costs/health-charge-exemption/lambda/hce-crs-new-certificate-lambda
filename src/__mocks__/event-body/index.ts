import hc2WithPartner from "./hc2-with-partner.json";
import hc2WithoutPartner from "./hc2-without-partner.json";
import hc3WithPartner from "./hc3-with-partner.json";
import hc3WithoutPartner from "./hc3-without-partner.json";

export { hc2WithPartner, hc2WithoutPartner, hc3WithPartner, hc3WithoutPartner };
