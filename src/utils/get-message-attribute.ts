import { SQSRecord } from "aws-lambda";

export const validateAndGetMessageAttribute = (
  record: SQSRecord,
  attribute: string,
): string => {
  const attributeValue = record.messageAttributes[attribute]?.stringValue;
  if (!attributeValue) {
    const message = `Message attribute ${attribute} is missing`;
    throw new Error(message);
  }
  return attributeValue;
};
