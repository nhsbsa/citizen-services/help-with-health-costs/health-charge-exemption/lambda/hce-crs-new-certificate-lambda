import { SQSEvent } from "aws-lambda";
import { logger } from "@nhsbsa/health-charge-exemption-npm-logging";
import { processRecord } from "./process-record";
import {
  BatchProcessor,
  EventType,
  processPartialResponse,
} from "@aws-lambda-powertools/batch";

export const processor = new BatchProcessor(EventType.SQS);

export async function handler(event: SQSEvent) {
  try {
    logger.info("hce-crs-change-handler-lambda");
    const { Records: records } = event;

    logger.info(`Events received count [${records.length}]`);

    return processPartialResponse(event, processRecord, processor);
  } catch (error) {
    logger.error("Failed to process hce-crs-change-handler-lambda \r\n", error);
    throw error;
  }
}
