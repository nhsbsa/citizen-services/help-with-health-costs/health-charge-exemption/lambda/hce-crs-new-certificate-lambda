import { validateAndGetMessageAttribute } from "../get-message-attribute";
import { mockData } from "../../__mocks__";

describe("validateAndGetMessageAttribute()", () => {
  test("should return the attribute value when it exists", () => {
    // given
    const record = mockData.certificateUpdateHc3SqsRecord;

    // when
    const attributeValue = validateAndGetMessageAttribute(
      record,
      "correlationId",
    );

    // then
    expect(attributeValue).toEqual(
      record.messageAttributes.correlationId.stringValue,
    );
  });

  test("should throw an error when the attribute is missing", () => {
    // given
    const record = mockData.certificateUpdateHc3SqsRecord;

    // when
    const attribute = "missingAttribute";

    // then
    expect(() => validateAndGetMessageAttribute(record, attribute)).toThrow(
      new Error(`Message attribute ${attribute} is missing`),
    );
  });
});
