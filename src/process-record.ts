import {
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/health-charge-exemption-npm-logging";
import { SQSRecord } from "aws-lambda";
import {
  handleCertificateUpdate,
  handleCitizenUpdate,
} from "./event-type-handler";
import { CONSTANTS, validateAndGetMessageAttribute } from "./utils";

const logger = loggerWithContext();

const EVENT_TYPES = {
  [CONSTANTS.CITIZEN_UPDATE]: handleCitizenUpdate,
  [CONSTANTS.CERTIFICATE_UPDATE]: handleCertificateUpdate,
};

const validateAndSetCorrelationId = (record: SQSRecord): string => {
  const correlationId = validateAndGetMessageAttribute(
    record,
    CONSTANTS.CORRELATION_ID,
  );
  setCorrelationId(correlationId);
  logger.info("Starting message processing", { messageId: record.messageId });
  return correlationId;
};

export async function processRecord(record: SQSRecord): Promise<void> {
  try {
    const correlationId = validateAndSetCorrelationId(record);

    const { eventType } = JSON.parse(record.body);

    // Get appropriate handler by eventType
    const eventHandler = EVENT_TYPES[eventType];

    if (!eventHandler) {
      logger.info(`Unsupported event type: ${eventType}, skipping call to CRS`);
    } else {
      await eventHandler(record, correlationId);
    }

    logger.info("Successfully processed the message", {
      messageId: record.messageId,
    });
  } catch (error) {
    logger.error("Failed to process message", {
      messageId: record.messageId,
      errorMessage: error.message,
    });
    throw error; // throwing here will mark the record as failed
  }
}
