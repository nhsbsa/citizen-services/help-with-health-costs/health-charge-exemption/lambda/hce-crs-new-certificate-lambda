import { Channel } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { getHeaders } from "../get-headers";

describe("getHeaders()", () => {
  test("should return the correct headers", () => {
    // given
    const correlationId = "223fc01c-368c-4636-8d5a-571375e52bec";

    // when
    const headers = getHeaders(correlationId);

    // then
    expect(headers).toEqual({
      channel: Channel.BATCH,
      "user-id": "CRS_EVENT",
      "correlation-id": correlationId,
    });
  });
});
