import { LisCertificateTypes } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { z } from "zod";

export const RecTypeMap: Record<LisCertificateTypes, string> = {
  LIS_HC2: "HC2",
  LIS_HC3: "HC3",
};

export const CrsAddressSchema = z.object({
  addressLine1: z.string(),
  addressLine2: z.string().optional(),
  addressLine3: z.string().optional(),
  addressLine4: z.string().optional(),
  postcode: z.string(),
});

export const CrsBaseRequestSchema = z.object({
  caseRefId: z.string(),
  applicantId: z.string(),
  title: z.string().optional().default(""),
  forename: z.string(),
  surname: z.string(),
  dateOfBirth: z.string(),
  ...CrsAddressSchema.shape,
});

export const CrsPostRequestSchema = CrsBaseRequestSchema.extend({
  recType: z.enum(["HC2", "HC3"] as const),
  validFrom: z.string(),
  validTo: z.string(),
  excessIncome: z.number().optional(),
  caseOwner: z.string(),
  submittedDate: z.string(),
  accept: z.boolean().optional().default(true),
  partnerForename: z.string().optional(),
  partnerSurname: z.string().optional(),
});

export const CrsUpdateRequestSchema = CrsBaseRequestSchema.extend({
  userId: z.string().default("HCE"),
  welshLanguage: z.number().optional().default(0),
  welshHC1: z.number().optional().default(0),
});

export type CrsPostRequest = z.infer<typeof CrsPostRequestSchema>;
export type CrsUpdateRequest = z.infer<typeof CrsUpdateRequestSchema>;
