import { getCitizen } from "../get-citizen";
import { CitizenApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockData } from "../../__mocks__";

let apiSpy: jest.SpyInstance;

beforeAll(() => {
  apiSpy = jest
    .spyOn(CitizenApi.prototype, "makeRequest")
    .mockResolvedValue({});
});

describe("getCitizen()", () => {
  test("should call CitizenApi with the correct parameters", async () => {
    // given
    const citizenId = "aa9d0167-5686-451f-ba5b-e7e52dc06e84";

    // when
    await getCitizen(citizenId, mockData.apiHeaders);

    // then
    expect(apiSpy).toHaveBeenCalledTimes(1);
    expect(apiSpy).toHaveBeenCalledWith({
      method: "GET",
      url: `/v1/citizens/${citizenId}`,
      headers: mockData.apiHeaders,
    });
  });

  test("should return the response from CitizenApi", async () => {
    // given
    apiSpy.mockResolvedValue(mockData.getCitizenResponseJson);
    const citizenId = mockData.getCitizenResponseJson.id;

    // when
    const result = await getCitizen(citizenId, mockData.apiHeaders);

    // then
    expect(result).toEqual(mockData.getCitizenResponseJson);
  });
});
