import { logger } from "@nhsbsa/health-charge-exemption-npm-logging";
process.env.NODE_ENV = "local";
process.env.outBoundApiTimeout = "10000";
process.env.logApiRequestAndResponse = "true";

export const infoSpy = jest.spyOn(logger, "info");
export const errorSpy = jest.spyOn(logger, "error");
