import { getActiveCertificates } from "../get-active-certificates";
import { getCertificates } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { mockData } from "../../__mocks__";
import { CONSTANTS } from "../constants";
import {
  CertificateResponse,
  CertificateStatus,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

jest.mock("@nhsbsa/health-charge-exemption-npm-utils-rest");

const mockGetCertificates = getCertificates as jest.MockedFunction<
  typeof getCertificates
>;
const headers = mockData.apiHeaders;
const citizenIds = mockData.citizenIds;
const today = mockData.currentDate;

beforeAll(() => {
  jest.useFakeTimers();
  jest.setSystemTime(new Date(today));
});

describe("getActiveCertificates", () => {
  test("should return an empty array when api response is an empty array ", async () => {
    // given
    mockGetCertificates.mockResolvedValue([]);

    // when
    const certificates = await getActiveCertificates(citizenIds, headers);

    // then
    verifyGetCertificatesApiCall();
    expect(certificates).toEqual([]);
  });

  test("should return an array containing certificates", async () => {
    // given
    const mockResponse = mockData.getCertificatesResponse;
    mockGetCertificates.mockResolvedValue(mockResponse);

    // when
    const certificates = await getActiveCertificates(citizenIds, headers);

    // then
    verifyGetCertificatesApiCall();
    expect(certificates).toEqual(mockResponse);
  });

  test("should handle get certificates api failure", async () => {
    // given
    mockGetCertificates.mockRejectedValue(new Error("API failure"));

    // when
    await expect(getActiveCertificates(citizenIds, headers)).rejects.toThrow(
      "API failure",
    );

    // then
    verifyGetCertificatesApiCall();
  });

  test("should filter out expired certificates and return only active, non-expired certificates", async () => {
    // given
    function getRelativeDate(baseDate, days = 0, years = 0) {
      const date = new Date(baseDate);
      date.setDate(date.getDate() + days);
      date.setFullYear(date.getFullYear() + years);
      return date;
    }
    const tomorrow = getRelativeDate(today, 1);
    const yesterday = getRelativeDate(today, -1);
    const oneYearFromNow = getRelativeDate(today, 0, 1);

    const mockResponse = [
      {
        id: "1",
        status: CertificateStatus.ACTIVE,
        endDate: tomorrow.toISOString(),
      },
      {
        id: "2",
        status: CertificateStatus.ACTIVE,
        endDate: yesterday.toISOString(),
      },
      { id: "3", status: CertificateStatus.ACTIVE, endDate: today },
      {
        id: "4",
        status: CertificateStatus.ACTIVE,
        endDate: oneYearFromNow.toISOString(),
      },
    ];
    mockGetCertificates.mockResolvedValue(
      mockResponse as CertificateResponse[],
    );

    // when
    const certificates = await getActiveCertificates(citizenIds, headers);

    // then
    verifyGetCertificatesApiCall();
    const expectedCertificates = mockResponse.filter((cert) => cert.id !== "2");
    expect(certificates).toEqual(expectedCertificates);
    expect(certificates).toHaveLength(3);
    expect(certificates.some((cert) => cert.id === "2")).toBeFalsy();
  });
});

const verifyGetCertificatesApiCall = () => {
  expect(getCertificates).toHaveBeenCalledTimes(1);
  expect(getCertificates).toHaveBeenCalledWith(
    CONSTANTS.CERTIFICATE_API_URL,
    {
      citizenIds,
      status: CertificateStatus.ACTIVE,
    },
    headers,
    CONSTANTS.CERTIFICATE_API_PAGE_SIZE,
  );
};
