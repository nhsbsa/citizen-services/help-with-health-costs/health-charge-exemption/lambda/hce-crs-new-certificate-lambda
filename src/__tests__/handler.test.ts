import { SQSEvent } from "aws-lambda";
import { handler, processor } from "../index";
import { processRecord } from "../process-record";
import * as powertools from "@aws-lambda-powertools/batch";
import { mockData } from "../__mocks__";

jest.mock("../process-record");

let event: SQSEvent;
const mockProcessRecord = processRecord as jest.MockedFunction<
  typeof processRecord
>;

let powertoolsSpy: jest.SpyInstance;

describe("handler()", () => {
  beforeEach(() => {
    jest.restoreAllMocks();
    powertoolsSpy = jest.spyOn(powertools, "processPartialResponse");
  });

  test.each([
    ["citizen update", mockData.citizenUpdateSqsEvent],
    ["certificate update with hc3", mockData.certificateUpdateHc3SqsEvent],
    ["certificate update with hc2", mockData.certificateUpdateHc2SqsEvent],
  ])(
    "should handle a '%s' event successfully without any error",
    async (_, event) => {
      // given / when
      await expect(handler(event)).resolves.not.toThrow();

      // then
      expect(mockProcessRecord).toHaveBeenCalledTimes(1);
      expect(mockProcessRecord.mock.calls[0][0]).toEqual(event.Records[0]);
      verifyCallToPowerToolsSpy(event);
    },
  );

  test("should handle an event with multiple sqs records successfully without any error", async () => {
    // given
    event = mockData.multipleRecordsSqsEvent;

    // when
    await expect(handler(event)).resolves.not.toThrow();

    // then
    expect(mockProcessRecord).toHaveBeenCalledTimes(2);
    expect(mockProcessRecord.mock.calls[0][0]).toEqual(event.Records[0]);
    expect(mockProcessRecord.mock.calls[1][0]).toEqual(event.Records[1]);
    verifyCallToPowerToolsSpy(event);
  });

  test("should process successfully when there is an error on a single record", async () => {
    // given
    event = mockData.multipleRecordsSqsEvent;
    const error = new Error("Partial failure");
    mockProcessRecord.mockRejectedValueOnce(error);

    // when / then
    await expect(handler(event)).resolves.not.toThrow();

    // then
    verifyCallToPowerToolsSpy(event);
  });

  test("should respond with an error when received an invalid event", async () => {
    // given
    event = "Invalid event" as unknown as SQSEvent;

    // when / then
    await expect(handler(event)).rejects.toThrow(
      new Error("Cannot read properties of undefined (reading 'length')"),
    );

    expect(mockProcessRecord).not.toHaveBeenCalled();
    expect(powertoolsSpy).not.toHaveBeenCalled();
  });
});

function verifyCallToPowerToolsSpy(event: SQSEvent) {
  expect(powertoolsSpy).toHaveBeenCalledTimes(1);
  expect(powertoolsSpy).toHaveBeenCalledWith(
    event,
    mockProcessRecord,
    processor,
  );
}
