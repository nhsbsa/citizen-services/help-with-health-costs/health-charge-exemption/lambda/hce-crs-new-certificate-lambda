import { CONSTANTS } from "../constants";

describe("CONSTANTS", () => {
  test("should have a CORRELATION_ID property & set to correct value", () => {
    expect(CONSTANTS).toHaveProperty("CORRELATION_ID");
    expect(CONSTANTS.CORRELATION_ID).toBe("correlationId");
  });

  test("should have an EVENT_TYPE property & set to correct value", () => {
    expect(CONSTANTS).toHaveProperty("EVENT_TYPE");
    expect(CONSTANTS.EVENT_TYPE).toBe("eventType");
  });
});
