import { getHeaders, getCitizen, getActiveCertificates } from "../utils";
import { SQSRecord } from "aws-lambda";
import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  CertificateResponse,
  CitizenUpdateSqsBody,
} from "@nhsbsa/health-charge-exemption-npm-common-models";
import { postCrsCitizen } from "../utils/crs/post-crs-citizen";
import { transformCertificateToCrsUpdateRequest } from "../utils/crs/crs-request-mapping";

const logger = loggerWithContext();

// This function is responsible for handling the citizen update event
export const handleCitizenUpdate = async (
  record: SQSRecord,
  correlationId: string,
) => {
  logger.info("Received and starting to process the citizen update event type");

  const headers = getHeaders(correlationId);
  const body: CitizenUpdateSqsBody = JSON.parse(record.body);
  const citizenId = body.citizenId;

  const citizenResponse = await getCitizen(citizenId, headers);

  if (!citizenResponse || !citizenResponse.id) {
    const message = `No citizen found for citizenId: ${citizenId}`;
    throw new Error(message);
  }

  const certificates: CertificateResponse[] = await getActiveCertificates(
    citizenId,
    headers,
  );

  await Promise.all(
    certificates.map(async (certificate) => {
      try {
        // we only directly update the main applicant as crs doesn't care for partner details in the same way - manual process.
        if (certificate.lowIncomeScheme?.partnerCitizenId === citizenId) {
          logger.info(
            `Skipping call to crs-citizen as partnerCitizenId match for citizenId: ${citizenId}`,
          );
          return;
        }
        const crsRequest = transformCertificateToCrsUpdateRequest(
          citizenResponse,
          certificate.reference,
        );
        await postCrsCitizen(crsRequest, headers);
      } catch (error) {
        logger.error(`Failed to update citizen with id: ${citizenId}`);
        throw error;
      }
    }),
  );
};
