import { handleCitizenUpdate } from "../handle-citizen-update";
import { getHeaders, getCitizen, getActiveCertificates } from "../../utils";
import { mockData } from "../../__mocks__";
import { postCrsCitizen } from "../../utils/crs/post-crs-citizen";
import { transformCertificateToCrsUpdateRequest } from "../../utils/crs/crs-request-mapping";
import { errorSpy, infoSpy } from "../../../test-setup/setup";

jest.mock("../../utils");
const mockGetHeaders = getHeaders as jest.MockedFunction<typeof getHeaders>;
const mockGetCitizen = getCitizen as jest.MockedFunction<typeof getCitizen>;
const mockGetActiveCertificates = getActiveCertificates as jest.MockedFunction<
  typeof getActiveCertificates
>;

jest.mock("../../utils/crs/post-crs-citizen");
const mockPostCrsCitizen = postCrsCitizen as jest.MockedFunction<
  typeof postCrsCitizen
>;

const citizenUpdateSqsRecord = mockData.citizenUpdateSqsRecord;
const citizenId = JSON.parse(citizenUpdateSqsRecord.body).citizenId;
const headers = mockData.apiHeaders;
const correlationId = mockData.correlationId;

describe("handleCitizenUpdate()", () => {
  beforeEach(() => {
    mockGetHeaders.mockReturnValue(headers);
  });

  afterEach(() => {
    expect(infoSpy).toHaveBeenCalledWith(
      "Received and starting to process the citizen update event type",
    );
  });

  test("should handle citizen update event successfully", async () => {
    // given
    mockGetCitizen.mockResolvedValue(mockData.getCitizenResponseJson);
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[0],
    ]);

    // when
    await expect(
      handleCitizenUpdate(citizenUpdateSqsRecord, correlationId),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(citizenId, headers);
    expect(mockPostCrsCitizen).toHaveBeenCalledTimes(1);
    expect(mockPostCrsCitizen).toHaveBeenNthCalledWith(
      1,
      transformCertificateToCrsUpdateRequest(
        mockData.getCitizenResponseJson,
        mockData.getCertificatesResponse[0].reference,
      ),
      headers,
    );
    expect(infoSpy).toHaveBeenCalledTimes(1);
  });

  test("should handle citizen update event successfully with multiple certificates", async () => {
    // given
    mockGetCitizen.mockResolvedValue(mockData.getCitizenResponseJson);
    mockGetActiveCertificates.mockResolvedValue(
      mockData.getCertificatesResponse,
    );

    // when
    await expect(
      handleCitizenUpdate(citizenUpdateSqsRecord, correlationId),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(citizenId, headers);
    expect(mockPostCrsCitizen).toHaveBeenCalledTimes(2);
    expect(mockPostCrsCitizen).toHaveBeenNthCalledWith(
      1,
      transformCertificateToCrsUpdateRequest(
        mockData.getCitizenResponseJson,
        mockData.getCertificatesResponse[0].reference,
      ),
      headers,
    );
    expect(mockPostCrsCitizen).toHaveBeenNthCalledWith(
      2,
      transformCertificateToCrsUpdateRequest(
        mockData.getCitizenResponseJson,
        mockData.getCertificatesResponse[1].reference,
      ),
      headers,
    );
    expect(infoSpy).toHaveBeenCalledTimes(1);
  });

  test("should ignore certificates where partnerCitizenId matches the event citizenId", async () => {
    // given
    const partnerCitizenId = "110841e3-e6fb-4191-8fd8-5674a5107c35";
    const skippingPostInfoLogMessage = `Skipping call to crs-citizen as partnerCitizenId match for citizenId: ${partnerCitizenId}`;
    mockGetCitizen.mockResolvedValue({
      ...mockData.getCitizenResponseJson,
      id: partnerCitizenId,
    });
    mockGetActiveCertificates.mockResolvedValue(
      mockData.getCertificatesResponse,
    );

    // when
    await expect(
      handleCitizenUpdate(
        {
          ...citizenUpdateSqsRecord,
          body: JSON.stringify({
            ...JSON.parse(citizenUpdateSqsRecord.body),
            citizenId: partnerCitizenId,
          }),
        },
        correlationId,
      ),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(partnerCitizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(
      partnerCitizenId,
      headers,
    );
    expect(mockPostCrsCitizen).not.toHaveBeenCalled();
    expect(infoSpy).toHaveBeenCalledTimes(3);
    expect(infoSpy).toHaveBeenNthCalledWith(2, skippingPostInfoLogMessage);
    expect(infoSpy).toHaveBeenNthCalledWith(3, skippingPostInfoLogMessage);
  });

  test("should throw an error when the citizen is not found", async () => {
    // given
    mockGetCitizen.mockResolvedValue(mockData.citizenApiNotFoundResponse);
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[0],
    ]);

    // when
    await expect(() =>
      handleCitizenUpdate(citizenUpdateSqsRecord, correlationId),
    ).rejects.toThrow("No citizen found for citizenId: " + citizenId);

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).not.toHaveBeenCalled();
    expect(mockPostCrsCitizen).not.toHaveBeenCalled();
  });

  test("should throw an error when the crs citizen update fails", async () => {
    // given
    mockGetCitizen.mockResolvedValue(mockData.getCitizenResponseJson);
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[0],
    ]);
    mockPostCrsCitizen.mockRejectedValue(new Error("Internal Server Error"));

    // when
    await expect(() =>
      handleCitizenUpdate(citizenUpdateSqsRecord, correlationId),
    ).rejects.toThrow("Internal Server Error");

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(citizenId, headers);
    expect(mockPostCrsCitizen).toHaveBeenCalledTimes(1);
    expect(mockPostCrsCitizen).toHaveBeenNthCalledWith(
      1,
      transformCertificateToCrsUpdateRequest(
        mockData.getCitizenResponseJson,
        mockData.getCertificatesResponse[0].reference,
      ),
      headers,
    );
    expect(errorSpy).toHaveBeenCalledTimes(1);
    expect(errorSpy).toHaveBeenCalledWith(
      `Failed to update citizen with id: ${citizenId}`,
    );
    expect(infoSpy).toHaveBeenCalledTimes(1);
  });
});
