import {
  CertificateResponse,
  CertificateType,
  CitizenGetResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";
import {
  CrsPostRequest,
  CrsPostRequestSchema,
  CrsUpdateRequest,
  CrsUpdateRequestSchema,
  RecTypeMap,
} from "./schemas";

export const transformCertificateToCrsRequest = (
  citizen: CitizenGetResponse,
  partner: CitizenGetResponse | undefined,
  certificate: CertificateResponse,
): CrsPostRequest => {
  const primaryAddress = citizen.addresses[0]; // Assume first address is primary

  const excessIncome =
    certificate.type !== CertificateType.LIS_HC2
      ? certificate.lowIncomeScheme?.excessIncome
      : undefined;

  return CrsPostRequestSchema.parse({
    caseRefId: certificate.reference,
    recType: RecTypeMap[certificate.type],
    validFrom: certificate.startDate,
    validTo: certificate.endDate,
    caseOwner: "HCE",
    submittedDate: certificate.applicationDate,
    applicantId: "HCE" + certificate.reference,
    forename: citizen.firstName,
    surname: citizen.lastName,
    dateOfBirth: citizen.dateOfBirth,
    addressLine1: primaryAddress.addressLine1,
    addressLine2: primaryAddress.addressLine2,
    addressLine3: primaryAddress.townOrCity,
    addressLine4: primaryAddress.country,
    postcode: primaryAddress.postcode,
    partnerForename: partner?.firstName,
    partnerSurname: partner?.lastName,
    excessIncome: excessIncome,
  });
};

export const transformCertificateToCrsUpdateRequest = (
  citizen: CitizenGetResponse,
  certificateReference: string,
): CrsUpdateRequest => {
  const primaryAddress = citizen.addresses[0]; // Assume first address is primary

  return CrsUpdateRequestSchema.parse({
    caseRefId: certificateReference,
    applicantId: "HCE" + certificateReference,
    forename: citizen.firstName,
    surname: citizen.lastName,
    dateOfBirth: citizen.dateOfBirth,
    addressLine1: primaryAddress.addressLine1,
    addressLine2: primaryAddress.addressLine2,
    addressLine3: primaryAddress.townOrCity,
    addressLine4: primaryAddress.country,
    postcode: primaryAddress.postcode,
  });
};
