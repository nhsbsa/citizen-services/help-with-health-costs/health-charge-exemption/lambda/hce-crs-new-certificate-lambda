import { generateMock } from "@anatine/zod-mock";
import { CrsPostRequestSchema, CrsUpdateRequestSchema } from "../schemas";
import { z } from "zod";

describe("CRS schema", () => {
  const validCrsPostMockData = generateMock(CrsPostRequestSchema);
  const validCrsUpdateMockData = generateMock(CrsUpdateRequestSchema);

  describe.each([
    [CrsPostRequestSchema, validCrsPostMockData],
    [CrsUpdateRequestSchema, validCrsUpdateMockData],
  ])("schema validation", (schema, mockData) => {
    it("should validate correct data per schema", () => {
      // given / when
      const result = schema.safeParse(mockData);

      // then
      expect(result.success).toBe(true);
    });

    it("should invalidate incorrect data when caseRefId is not number and recType is not HC2 OR HC3", () => {
      // given
      const invalidData = {
        ...mockData,
        caseRefId: 123,
        recType: "InvalidType",
      };

      // when
      const result = schema.safeParse(invalidData);

      // then
      expect(result.success).toBe(false);
      // needed for result.error
      if (!result.success) {
        expect(result.error.issues.length).toBeGreaterThan(0);
        expect(result.error.issues).toContainEqual(
          expect.objectContaining({ path: ["caseRefId"] }),
        );
        if (schema.shape["recType"]) {
          expect(result.error.issues).toContainEqual(
            expect.objectContaining({ path: ["recType"] }),
          );
        }
      }
    });
  });

  it("should allow optional fields for CrsPostRequestSchema to be omitted when parsed", () => {
    // given
    const optionalFields = [
      "excessIncome",
      "title",
      "accept",
      "partnerForename",
      "partnerSurname",
      "addressLine2",
      "addressLine3",
      "addressLine4",
    ] as const;
    const partialSchema = createSchemaWithoutFields(
      CrsPostRequestSchema,
      optionalFields,
    );
    const dataWithOptionalFieldsOmitted = generateMock(partialSchema);

    // when
    const result = CrsPostRequestSchema.safeParse(
      dataWithOptionalFieldsOmitted,
    );

    // then
    expect(result.success).toBe(true);
    Object.keys(optionalFields).forEach((field) => {
      expect(dataWithOptionalFieldsOmitted).not.toHaveProperty(field);
    });
  });

  it("should apply default values for CrsPostRequestSchema for title and accept when not provided", () => {
    // given
    const fieldsToOmit = ["title", "accept"] as const;
    const partialSchema = createSchemaWithoutFields(
      CrsPostRequestSchema,
      fieldsToOmit,
    );
    const dataWithoutTitleAndAccept = generateMock(partialSchema);

    // when
    const result = CrsPostRequestSchema.safeParse(dataWithoutTitleAndAccept);

    // then
    expect(result.success).toBe(true);
    if (result.success) {
      expect(result.data.title).toBe("");
      expect(result.data.accept).toBe(true);
    }
  });

  it("should apply default values for CrsUpdateRequestSchema for optional fields when not provided", () => {
    // given
    const fieldsToOmit = [
      "userId",
      "title",
      "welshLanguage",
      "welshHC1",
    ] as const;
    const partialSchema = createSchemaWithoutFields(
      CrsUpdateRequestSchema,
      fieldsToOmit,
    );
    const dataWithoutOptionalFields = generateMock(partialSchema);

    // when
    const result = CrsUpdateRequestSchema.safeParse(dataWithoutOptionalFields);

    // then
    expect(result.success).toBe(true);
    if (result.success) {
      expect(result.data.userId).toBe("HCE");
      expect(result.data.title).toBe("");
      expect(result.data.welshLanguage).toBe(0);
      expect(result.data.welshHC1).toBe(0);
    }
  });

  it("should allow optional fields for CrsUpdateRequestSchema to be omitted when parsed", () => {
    // given
    const optionalFields = [
      "addressLine2",
      "addressLine3",
      "addressLine4",
    ] as const;
    const partialSchema = createSchemaWithoutFields(
      CrsUpdateRequestSchema,
      optionalFields,
    );
    const dataWithOptionalFieldsOmitted = generateMock(partialSchema);

    // when
    const result = CrsUpdateRequestSchema.safeParse(
      dataWithOptionalFieldsOmitted,
    );

    // then
    expect(result.success).toBe(true);
    Object.keys(optionalFields).forEach((field) => {
      expect(dataWithOptionalFieldsOmitted).not.toHaveProperty(field);
    });
  });
});

// function to create a schema with optional fields
function createSchemaWithoutFields<T extends z.ZodTypeAny>(
  schema,
  fieldsToOmit: ReadonlyArray<string>,
): z.ZodType<Partial<z.infer<T>>> {
  // removes all optional fields, favoured over delete to avoid TS error on defined keys
  const omitObject = fieldsToOmit.reduce(
    (acc, field) => ({ ...acc, [field]: true }),
    {},
  );
  return schema.omit(omitObject).partial();
}
