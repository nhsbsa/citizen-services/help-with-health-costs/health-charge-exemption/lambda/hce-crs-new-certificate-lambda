export const CONSTANTS = Object.freeze({
  CORRELATION_ID: "correlationId",
  EVENT_TYPE: "eventType",
  CERTIFICATE_UPDATE: "issue-certificate",
  CITIZEN_UPDATE: "citizen-update",
  CERTIFICATE_API_URL: "/v1/certificates/search/findByCitizens",
  CERTIFICATE_API_PAGE_SIZE: 500,
});
