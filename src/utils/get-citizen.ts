import {
  CitizenApi,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import { CitizenGetResponse } from "@nhsbsa/health-charge-exemption-npm-common-models";

const logger = loggerWithContext();

export const getCitizen = async (
  citizenId: string,
  headers: MandatoryHeaders,
): Promise<CitizenGetResponse> => {
  logger.info("Retrieving from citizen api", { citizenId });

  return new CitizenApi().makeRequest({
    method: "GET",
    url: `/v1/citizens/${citizenId}`,
    headers,
  });
};
