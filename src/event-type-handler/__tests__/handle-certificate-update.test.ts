import { handleCertificateUpdate } from "../handle-certificate-update";
import { getHeaders, getCitizen, getActiveCertificates } from "../../utils";
import { mockData } from "../../__mocks__";
import { CertificateUpdateSqsBody } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { postCrsCases } from "../../utils/crs/post-crs-cases";
import { transformCertificateToCrsRequest } from "../../utils/crs/crs-request-mapping";
import { errorSpy, infoSpy } from "../../../test-setup/setup";

jest.mock("../../utils");
const mockGetHeaders = getHeaders as jest.MockedFunction<typeof getHeaders>;
const mockGetCitizen = getCitizen as jest.MockedFunction<typeof getCitizen>;
const mockGetActiveCertificates = getActiveCertificates as jest.MockedFunction<
  typeof getActiveCertificates
>;
jest.mock("../../utils/crs/post-crs-cases");
const mockPostCrs = postCrsCases as jest.MockedFunction<typeof postCrsCases>;

const headers = mockData.apiHeaders;
const getCitizenResponseJson = mockData.getCitizenResponseJson;
const correlationId = mockData.correlationId;

beforeEach(() => {
  mockGetHeaders.mockReturnValue(headers);
  mockGetCitizen.mockResolvedValue(getCitizenResponseJson);
});

describe("handleCertificateUpdate()", () => {
  afterEach(() => {
    expect(infoSpy).toHaveBeenCalledTimes(1);
    expect(infoSpy).toHaveBeenCalledWith(
      "Received and starting to process the certificate update event type",
    );
  });

  test("should handle certificates with LIS_HC3 type & contains partner", async () => {
    // given
    const body = mockData.eventBody.hc3WithPartner;
    const hc3RecordWithPartner = {
      ...mockData.certificateUpdateHc3SqsRecord,
      body: JSON.stringify(body),
    };
    const citizenId = body.citizenId;
    const partnerCitizenId = body.lowIncomeScheme?.partnerCitizenId;
    mockGetCitizen
      .mockImplementationOnce(() =>
        Promise.resolve(mockData.getCitizenResponseJson),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockData.getPartnerCitizenResponseJson),
      );
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[0],
    ]);

    // when
    await expect(
      handleCertificateUpdate(hc3RecordWithPartner, correlationId),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(2);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetCitizen).toHaveBeenCalledWith(partnerCitizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(
      `${citizenId},${partnerCitizenId}`,
      headers,
    );
    expect(mockPostCrs).toHaveBeenCalledTimes(1);
    const expectedCrsRequest = transformCertificateToCrsRequest(
      getCitizenResponseJson,
      mockData.getPartnerCitizenResponseJson,
      mockData.getCertificatesResponse[0],
    );
    expect(mockPostCrs).toHaveBeenCalledWith(expectedCrsRequest, headers);
  });

  test("should handle certificates with LIS_HC3 type & without partner", async () => {
    // given
    const body = mockData.eventBody.hc3WithoutPartner;
    const hc3RecordWithoutPartner = {
      ...mockData.certificateUpdateHc3SqsRecord,
      body: JSON.stringify(body),
    };
    const citizenId = body.citizenId;
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[0],
    ]);

    // when
    await expect(
      handleCertificateUpdate(hc3RecordWithoutPartner, correlationId),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(citizenId, headers);
    expect(mockPostCrs).toHaveBeenCalledTimes(1);
    const expectedCrsRequest = transformCertificateToCrsRequest(
      getCitizenResponseJson,
      undefined,
      mockData.getCertificatesResponse[0],
    );
    expect(mockPostCrs).toHaveBeenCalledWith(expectedCrsRequest, headers);
  });

  test("should handle certificates with LIS_HC2 type & contains partner", async () => {
    // given
    const body = mockData.eventBody.hc2WithPartner;
    const hc2RecordWithPartner = {
      ...mockData.certificateUpdateHc2SqsRecord,
      body: JSON.stringify(body),
    };
    const citizenId = body.citizenId;
    const partnerCitizenId = body.lowIncomeScheme?.partnerCitizenId;
    mockGetCitizen
      .mockImplementationOnce(() =>
        Promise.resolve(mockData.getCitizenResponseJson),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockData.getPartnerCitizenResponseJson),
      );
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[1],
    ]);

    // when
    await expect(
      handleCertificateUpdate(hc2RecordWithPartner, mockData.correlationId),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(2);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetCitizen).toHaveBeenCalledWith(partnerCitizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(
      `${citizenId},${partnerCitizenId}`,
      headers,
    );
    expect(mockPostCrs).toHaveBeenCalledTimes(1);
    const expectedCrsRequest = transformCertificateToCrsRequest(
      getCitizenResponseJson,
      mockData.getPartnerCitizenResponseJson,
      mockData.getCertificatesResponse[1],
    );
    expect(mockPostCrs).toHaveBeenCalledWith(expectedCrsRequest, headers);
  });

  test("should handle certificates with type LIS_HC2 & without partner", async () => {
    // given
    const body = mockData.eventBody.hc2WithoutPartner;
    const hc2RecordWithoutPartner = {
      ...mockData.certificateUpdateHc2SqsRecord,
      body: JSON.stringify(body),
    };
    const citizenId = body.citizenId;
    mockGetCitizen.mockImplementationOnce(() =>
      Promise.resolve(mockData.getCitizenResponseJson),
    );
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[1],
    ]);

    // when
    await expect(
      handleCertificateUpdate(hc2RecordWithoutPartner, correlationId),
    ).resolves.not.toThrow();

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(citizenId, headers);
    expect(mockPostCrs).toHaveBeenCalledTimes(1);
    const expectedCrsRequest = transformCertificateToCrsRequest(
      getCitizenResponseJson,
      undefined,
      mockData.getCertificatesResponse[1],
    );
    expect(mockPostCrs).toHaveBeenCalledWith(expectedCrsRequest, headers);
  });

  test("should throw an error when the citizen is not found", async () => {
    // given
    mockGetCitizen.mockResolvedValue(mockData.citizenApiNotFoundResponse);
    const body: CertificateUpdateSqsBody = JSON.parse(
      mockData.certificateUpdateHc3SqsRecord.body,
    );
    const citizenId = body.citizenId;

    // when
    await expect(() =>
      handleCertificateUpdate(
        mockData.certificateUpdateHc3SqsRecord,
        correlationId,
      ),
    ).rejects.toThrow("No citizen found for citizenId: " + citizenId);

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(1);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetActiveCertificates).not.toHaveBeenCalled();
    expect(mockPostCrs).not.toHaveBeenCalled();
  });

  test("should throw an error when the partner is not found", async () => {
    // given
    mockGetCitizen
      .mockResolvedValueOnce(mockData.getCitizenResponseJson)
      .mockResolvedValueOnce(mockData.citizenApiNotFoundResponse);
    const body: CertificateUpdateSqsBody = JSON.parse(
      mockData.certificateUpdateHc3SqsRecord.body,
    );
    const citizenId = body.citizenId;
    const partnerCitizenId = body.lowIncomeScheme?.partnerCitizenId;

    // when
    await expect(() =>
      handleCertificateUpdate(
        mockData.certificateUpdateHc3SqsRecord,
        correlationId,
      ),
    ).rejects.toThrow(
      "No partner found for partnerCitizenId: " + partnerCitizenId,
    );

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(2);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetCitizen).toHaveBeenCalledWith(partnerCitizenId, headers);
    expect(mockGetActiveCertificates).not.toHaveBeenCalled();
    expect(mockPostCrs).not.toHaveBeenCalled();
  });

  test.each([["invalid-certificate-type"], ["HRT_PPC"]])(
    `should throw an error when certificate type is %s`,
    async (type) => {
      // given
      const invalidCertificateTypeSqsRecord = {
        ...mockData.certificateUpdateHc3SqsRecord,
      };
      const body = JSON.parse(invalidCertificateTypeSqsRecord.body);
      body.certificate.type = type;
      invalidCertificateTypeSqsRecord.body = JSON.stringify(body);

      // when
      await expect(() =>
        handleCertificateUpdate(
          invalidCertificateTypeSqsRecord,
          mockData.correlationId,
        ),
      ).rejects.toThrow(`Received unsupported certificate type: ${type}`);

      // then
      expect(mockGetHeaders).toHaveBeenCalledTimes(1);
      expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
      expect(mockGetCitizen).not.toHaveBeenCalled();
      expect(mockGetActiveCertificates).not.toHaveBeenCalled();
      expect(mockPostCrs).not.toHaveBeenCalled();
    },
  );

  test("should throw an error when crs fails", async () => {
    // given
    const body: CertificateUpdateSqsBody = JSON.parse(
      mockData.certificateUpdateHc3SqsRecord.body,
    );
    const citizenId = body.citizenId;
    const partnerCitizenId = body.lowIncomeScheme?.partnerCitizenId;
    mockGetCitizen
      .mockImplementationOnce(() =>
        Promise.resolve(mockData.getCitizenResponseJson),
      )
      .mockImplementationOnce(() =>
        Promise.resolve(mockData.getPartnerCitizenResponseJson),
      );
    mockGetActiveCertificates.mockResolvedValue([
      mockData.getCertificatesResponse[1],
    ]);
    mockPostCrs.mockRejectedValue(new Error("Internal Server Error"));

    // when
    await expect(() =>
      handleCertificateUpdate(
        mockData.certificateUpdateHc3SqsRecord,
        correlationId,
      ),
    ).rejects.toThrow("Internal Server Error");

    // then
    expect(mockGetHeaders).toHaveBeenCalledTimes(1);
    expect(mockGetHeaders).toHaveBeenCalledWith(correlationId);
    expect(mockGetCitizen).toHaveBeenCalledTimes(2);
    expect(mockGetCitizen).toHaveBeenCalledWith(citizenId, headers);
    expect(mockGetCitizen).toHaveBeenCalledWith(partnerCitizenId, headers);
    expect(mockGetActiveCertificates).toHaveBeenCalledTimes(1);
    expect(mockGetActiveCertificates).toHaveBeenCalledWith(
      `${citizenId},${partnerCitizenId}`,
      headers,
    );
    expect(mockPostCrs).toHaveBeenCalledTimes(1);
    const expectedCrsRequest = transformCertificateToCrsRequest(
      getCitizenResponseJson,
      mockData.getPartnerCitizenResponseJson,
      mockData.getCertificatesResponse[1],
    );
    expect(mockPostCrs).toHaveBeenCalledWith(expectedCrsRequest, headers);
    expect(errorSpy).toHaveBeenCalledTimes(1);
    expect(errorSpy).toHaveBeenCalledWith(
      `Failed to POST to the CRS API for certificate id: ${mockData.getCertificatesResponse[1].id}`,
    );
  });
});
