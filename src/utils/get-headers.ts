import { Channel } from "@nhsbsa/health-charge-exemption-npm-common-models";

export const getHeaders = (correlationId: string) => ({
  channel: Channel.BATCH,
  "user-id": "CRS_EVENT",
  "correlation-id": correlationId,
});
