import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import { CONSTANTS } from "./constants";
import {
  CertificateStatus,
  CertificateResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";
import {
  MandatoryHeaders,
  getCertificates,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const logger = loggerWithContext();

export const getActiveCertificates = async (
  citizenIds: string,
  headers: MandatoryHeaders,
) => {
  const queryParameters = {
    citizenIds,
    status: CertificateStatus.ACTIVE,
  };

  const certificates: CertificateResponse[] = await getCertificates(
    CONSTANTS.CERTIFICATE_API_URL,
    queryParameters,
    headers,
    CONSTANTS.CERTIFICATE_API_PAGE_SIZE,
  );

  const today = new Date().toISOString().slice(0, 10);
  const activeCertificates = certificates.filter(
    (certificate) => new Date(certificate.endDate).toISOString() > today,
  );

  if (!activeCertificates || activeCertificates.length === 0) {
    logger.info("No active certificates have been found ", { citizenIds });
    return [];
  }

  return activeCertificates;
};
