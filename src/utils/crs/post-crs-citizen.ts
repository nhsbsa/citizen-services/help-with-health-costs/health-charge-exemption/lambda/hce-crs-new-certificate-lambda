import { loggerWithContext } from "@nhsbsa/health-charge-exemption-npm-logging";
import {
  CrsApi,
  MandatoryHeaders,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { HttpRequestMethod } from "@nhsbsa/health-charge-exemption-npm-common-models";
import { CrsUpdateRequest } from "./schemas";

const logger = loggerWithContext();

export const postCrsCitizen = async (
  crsUpdateRequest: CrsUpdateRequest,
  headers: MandatoryHeaders,
): Promise<unknown> => {
  logger.info(
    "Posting update to the CRS API for certificate",
    crsUpdateRequest.caseRefId,
  );

  return new CrsApi().makeRequest({
    method: HttpRequestMethod.POST,
    url: `/crs-citizen`,
    headers,
    data: crsUpdateRequest,
  });
};
