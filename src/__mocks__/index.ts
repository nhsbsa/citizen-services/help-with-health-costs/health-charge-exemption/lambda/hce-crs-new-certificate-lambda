import mockGetCitizenResponseJson from "./api-response/get-citizen.json";
import mockGetCertificatesResponseJson from "./api-response/get-certificates.json";
import mockApiHeadersJson from "./api-headers.json";
import mockCitizenUpdateSqsRecordJson from "./sqs/citizen-update.json";
import mockCertificateUpdateSqsRecordHc3Json from "./sqs/certificate-update-hc3.json";
import mockCertificateUpdateSqsRecordHc2Json from "./sqs/certificate-update-hc2.json";
import mockCitizenUpdateSqsEvent from "../../events/event-citizen-update.json";
import mockCertificateUpdateHc2SqsEvent from "../../events/event-certificate-update-hc2.json";
import mockCertificateUpdateHc3SqsEvent from "../../events/event-certificate-update-hc3.json";
import mockMultipleSqsEvent from "../../events/event-multiple-records.json";
import * as eventBody from "./event-body";
import { MandatoryHeaders } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { SQSRecord, SQSEvent } from "aws-lambda";
import {
  CitizenGetResponse,
  CertificateResponse,
} from "@nhsbsa/health-charge-exemption-npm-common-models";

export const mockData = {
  apiHeaders: mockApiHeadersJson as MandatoryHeaders,
  citizenUpdateSqsRecord: mockCitizenUpdateSqsRecordJson as SQSRecord,
  citizenUpdateSqsEvent: mockCitizenUpdateSqsEvent as unknown as SQSEvent,
  getCitizenResponseJson: mockGetCitizenResponseJson as CitizenGetResponse,
  getPartnerCitizenResponseJson: {
    ...mockGetCitizenResponseJson,
    firstName: "Jane",
    lastName: "Doe",
  } as CitizenGetResponse,
  citizenApiNotFoundResponse: {} as CitizenGetResponse,
  certificateUpdateHc3SqsRecord:
    mockCertificateUpdateSqsRecordHc3Json as SQSRecord,
  certificateUpdateHc2SqsRecord:
    mockCertificateUpdateSqsRecordHc2Json as SQSRecord,
  correlationId: "223fc01c-368c-4636-8d5a-571375e52bec",
  certificateUpdateHc3SqsEvent:
    mockCertificateUpdateHc3SqsEvent as unknown as SQSEvent,
  certificateUpdateHc2SqsEvent:
    mockCertificateUpdateHc2SqsEvent as unknown as SQSEvent,
  multipleRecordsSqsEvent: mockMultipleSqsEvent as unknown as SQSEvent,
  eventBody,
  citizenIds:
    "aa9d0167-5686-451f-ba5b-e7e52dc06e84,aa9d0167-5686-451f-ba5b-e7e52dc06e85",
  currentDate: "2024-07-23",
  getCertificatesResponse:
    mockGetCertificatesResponseJson as CertificateResponse[],
};
